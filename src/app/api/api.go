package main

import (
	"app/api/manager"
	"app/api/resource"
	"app/api/service/db"
	"flag"
	"fmt"
	"log"
	"net/http"

	"github.com/go-chi/chi"
)

func registerServer(dbstring string) http.Handler {
	r := chi.NewRouter()

	msql, err := db.NewMySQL(dbstring)
	if err != nil {
		panic(err)
	}

	r.Route("/users", resource.UserRegister(manager.NewUserManager(msql)))
	r.Route("/health", resource.HealthRegister(manager.NewHealthManager(msql)))
	r.Route("/auth", resource.AuthRegister(manager.NewAuthManager("secret")))

	return r
}

func main() {
	var port string
	flag.StringVar(&port, "port", "8080", "set a custom port")

	log.Printf("api running on :%s", port)
	log.Fatalln(http.ListenAndServe(fmt.Sprintf(":%s", port), registerServer("root@/gu_pa?charset=utf8")))
}
