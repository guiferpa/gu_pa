package db

type errRowNotFound struct{}

func (e *errRowNotFound) Error() string {
	return "row not found"
}

var ErrRowNotFound = &errRowNotFound{}

type UserPayload struct {
	LastName  string
	FirstName string
	Address   string
	Email     string
	Telephone string
}

type UserInfo struct {
	ID        string
	LastName  string
	FirstName string
	Address   string
	Email     string
	Telephone string
}

type ReadUserListOptions struct {
	Offset int
	Limit  int
}

type Database interface {
	CreateUser(user UserPayload) error
	ReadUserList(opts ReadUserListOptions) ([]UserInfo, error)
	ReadUser(ID string) (UserInfo, error)
	UpdateUser(ID string, user UserPayload) (int, error)
	DeleteUser(ID string) (int, error)
	Ping() error
}
