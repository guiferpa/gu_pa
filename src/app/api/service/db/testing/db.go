package testing

import "app/api/service/db"

type DB struct {
	MockCreateUser   func(user db.UserPayload) error
	MockReadUserList func(opts db.ReadUserListOptions) ([]db.UserInfo, error)
	MockReadUser     func(ID string) (db.UserInfo, error)
	MockUpdateUser   func(ID string, user db.UserPayload) (int, error)
	MockDeleteUser   func(ID string) (int, error)
	MockPing         func() error
}

func (d *DB) CreateUser(user db.UserPayload) error {
	if d.MockCreateUser != nil {
		return d.MockCreateUser(user)
	}
	return nil
}

func (d *DB) ReadUserList(opts db.ReadUserListOptions) ([]db.UserInfo, error) {
	if d.MockReadUserList != nil {
		return d.MockReadUserList(opts)
	}
	return make([]db.UserInfo, 0), nil
}

func (d *DB) ReadUser(ID string) (db.UserInfo, error) {
	if d.MockReadUser != nil {
		return d.MockReadUser(ID)
	}
	return db.UserInfo{}, nil
}

func (d *DB) UpdateUser(ID string, user db.UserPayload) (int, error) {
	if d.MockUpdateUser != nil {
		return d.MockUpdateUser(ID, user)
	}
	return 0, nil
}

func (d *DB) DeleteUser(ID string) (int, error) {
	if d.MockDeleteUser != nil {
		return d.MockDeleteUser(ID)
	}
	return 0, nil
}

func (d *DB) Ping() error {
	if d.MockPing != nil {
		return d.MockPing()
	}
	return nil
}
