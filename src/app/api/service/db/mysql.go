package db

import (
	"database/sql"

	// importing the driver for sql package
	_ "github.com/go-sql-driver/mysql"
)

type MySQL struct {
	connstring string
}

func (msql *MySQL) CreateUser(user UserPayload) error {
	dbase, err := sql.Open("mysql", msql.connstring)
	if err != nil {
		return err
	}
	defer dbase.Close()

	stmt, err := dbase.Prepare("INSERT INTO users SET last_name=?,first_name=?,address=?,email=?,telephone=?")
	if err != nil {
		return err
	}

	if _, err = stmt.Exec(user.LastName, user.FirstName, user.Address, user.Email, user.Email); err != nil {
		return err
	}

	return nil
}

func (msql *MySQL) ReadUserList(opts ReadUserListOptions) ([]UserInfo, error) {
	dbase, err := sql.Open("mysql", msql.connstring)
	if err != nil {
		return nil, err
	}
	defer dbase.Close()

	rows, err := dbase.Query("SELECT * FROM users LIMIT ?, ?", opts.Offset, opts.Limit)
	if err != nil {
		return nil, err
	}

	users := make([]UserInfo, 0)
	for rows.Next() {
		var ID, lname, fname, addr, email, tel string
		if err = rows.Scan(&ID, &lname, &fname, &addr, &email, &tel); err != nil {
			return users, err
		}
		users = append(users, UserInfo{
			ID: ID, LastName: lname, FirstName: fname, Address: addr, Email: email, Telephone: tel,
		})
	}
	return users, nil
}

func (msql *MySQL) ReadUser(ID string) (UserInfo, error) {
	dbase, err := sql.Open("mysql", msql.connstring)
	if err != nil {
		return UserInfo{}, err
	}
	defer dbase.Close()

	row := dbase.QueryRow("SELECT * FROM users WHERE id=?", ID)

	var lname, fname, addr, email, tel string
	if err = row.Scan(&ID, &lname, &fname, &addr, &email, &tel); err != nil {
		if err == sql.ErrNoRows {
			return UserInfo{}, ErrRowNotFound
		}
		return UserInfo{}, err
	}

	return UserInfo{
		ID: ID, LastName: lname, FirstName: fname, Address: addr, Email: email, Telephone: tel,
	}, nil
}

func (msql *MySQL) UpdateUser(ID string, user UserPayload) (int, error) {
	dbase, err := sql.Open("mysql", msql.connstring)
	if err != nil {
		return 0, err
	}
	defer dbase.Close()

	stmt, err := dbase.Prepare("UPDATE users SET last_name=?,first_name=?,address=?,email=?,telephone=? WHERE id=?")
	if err != nil {
		return 0, err
	}

	result, err := stmt.Exec(user.LastName, user.FirstName, user.Address, user.Email, user.Telephone, ID)
	if err != nil {
		return 0, err
	}

	rows, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}

	return int(rows), nil
}

func (msql *MySQL) DeleteUser(ID string) (int, error) {
	dbase, err := sql.Open("mysql", msql.connstring)
	if err != nil {
		return 0, err
	}
	defer dbase.Close()

	stmt, err := dbase.Prepare("DELETE FROM users WHERE id=?")
	if err != nil {
		return 0, err
	}

	result, err := stmt.Exec(ID)
	if err != nil {
		return 0, err
	}

	rows, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}

	return int(rows), nil
}

func (msql *MySQL) Ping() error {
	conn, err := sql.Open("mysql", msql.connstring)
	if err != nil {
		return err
	}
	defer conn.Close()

	return conn.Ping()
}

func NewMySQL(connstring string) (*MySQL, error) {
	conn, err := sql.Open("mysql", connstring)
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	if err = conn.Ping(); err != nil {
		return nil, err
	}

	return &MySQL{connstring: connstring}, nil
}
