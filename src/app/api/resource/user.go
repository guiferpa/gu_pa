package resource

import (
	"app/api/manager"
	"common/body/rule"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"

	bvalidate "common/body"

	"github.com/go-chi/chi"
)

type user struct {
	m manager.User
}

type userPayload struct {
	LastName  string `json:"last_name" validate:"required=true min_bound=3 max_bound=50"`
	FirstName string `json:"first_name" validate:"required=true min_bound=3 max_bound=30"`
	Address   string `json:"address" validate:"required=true min_bound=3 max_bound=100"`
	Email     string `json:"email" validate:"required=true min_bound=3 max_bound=50"`
	Telephone string `json:"telephone" validate:"required=true min_bound=3 max_bound=30"`
}

type userInfo struct {
	ID        string `json:"id"`
	LastName  string `json:"last_name"`
	FirstName string `json:"first_name"`
	Address   string `json:"address"`
	Email     string `json:"email"`
	Telephone string `json:"telephone"`
}

type errUserNotFound struct {
	ID      string `json:"id"`
	Message string `json:"message"`
}

type errUserUnprocessableEntity struct {
	Field  string `json:"field"`
	Reason string `json:"reason"`
}

func (euue *errUserUnprocessableEntity) Error() string {
	return euue.Reason
}

func getManagerUserPayload(b userPayload) manager.UserPayload {
	return manager.UserPayload{
		LastName:  b.LastName,
		FirstName: b.FirstName,
		Address:   b.Address,
		Email:     b.Email,
		Telephone: b.Telephone,
	}
}

func getUserInfo(u manager.UserInfo) userInfo {
	return userInfo{
		ID:        u.ID,
		LastName:  u.LastName,
		FirstName: u.FirstName,
		Address:   u.Address,
		Email:     u.Email,
		Telephone: u.Telephone,
	}
}

func getUsersInfo(us []manager.UserInfo) []userInfo {
	uis := make([]userInfo, 0)
	for _, u := range us {
		uis = append(uis, getUserInfo(u))
	}
	return uis
}

func validateBody(b userPayload) error {
	validated, err := bvalidate.Validate(b)
	if !validated {
		return &errUserUnprocessableEntity{Field: "none", Reason: "Unknown"}
	}
	if err != nil {
		switch ce := err.(type) {
		case *rule.ErrMinBound:
		case *rule.ErrMaxBound:
		case *rule.ErrRequired:
			errbody := &errUserUnprocessableEntity{Field: ce.Field, Reason: ce.Error()}
			return errbody
		}
	}
	return nil
}

func (u *user) create(w http.ResponseWriter, r *http.Request) {
	logger := log.New(os.Stderr, fmt.Sprintf("%s - %s :: ", r.Method, r.URL.Path), log.Ldate|log.Ltime|log.Lshortfile)

	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	var body userPayload
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		logger.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if err := validateBody(body); err != nil {
		logger.Println(err)
		w.WriteHeader(http.StatusUnprocessableEntity)
		if err = json.NewEncoder(w).Encode(err); err != nil {
			logger.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		return
	}

	if err := u.m.Create(getManagerUserPayload(body)); err != nil {
		logger.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusCreated)
	logger.Printf("User created successful")
}

func (u *user) list(w http.ResponseWriter, r *http.Request) {
	logger := log.New(os.Stderr, fmt.Sprintf("%s - %s :: ", r.Method, r.URL.Path), log.Ldate|log.Ltime|log.Lshortfile)

	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	offset, err := strconv.Atoi(r.URL.Query().Get("offset"))
	if err != nil {
		logger.Println("When parse query string [offset]:", offset)
	}

	limit, err := strconv.Atoi(r.URL.Query().Get("limit"))
	if err != nil {
		logger.Println("When parse query string [limit]:", err)
	}

	opts := manager.UserListOptions{Offset: offset, Limit: limit}
	userList, err := u.m.List(opts)
	if err != nil {
		logger.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if err := json.NewEncoder(w).Encode(getUsersInfo(userList)); err != nil {
		logger.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	logger.Println("Users listed successful")
}

func (u *user) get(w http.ResponseWriter, r *http.Request) {
	logger := log.New(os.Stderr, fmt.Sprintf("%s - %s :: ", r.Method, r.URL.Path), log.Ldate|log.Ltime|log.Lshortfile)

	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	ID := chi.URLParam(r, "id")
	user, err := u.m.Get(ID)
	if err != nil {
		if ce, ok := err.(*manager.ErrUserNotFound); ok {
			logger.Println(ce)
			b := errUserNotFound{ID: ce.ID, Message: ce.Error()}
			w.WriteHeader(http.StatusNotFound)
			if err = json.NewEncoder(w).Encode(b); err != nil {
				logger.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
			}
			return
		}
		logger.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if err := json.NewEncoder(w).Encode(getUserInfo(user)); err != nil {
		logger.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	logger.Printf("User %v retrieved successful", ID)
}

func (u *user) update(w http.ResponseWriter, r *http.Request) {
	logger := log.New(os.Stderr, fmt.Sprintf("%s - %s :: ", r.Method, r.URL.Path), log.Ldate|log.Ltime|log.Lshortfile)

	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	var body userPayload
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		logger.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if err := validateBody(body); err != nil {
		logger.Println(err)
		w.WriteHeader(http.StatusUnprocessableEntity)
		if err = json.NewEncoder(w).Encode(err); err != nil {
			logger.Println(err)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		return
	}

	ID := chi.URLParam(r, "id")
	if err := u.m.Update(ID, getManagerUserPayload(body)); err != nil {
		if ce, ok := err.(*manager.ErrUserNotFound); ok {
			b := errUserNotFound{ID: ce.ID, Message: ce.Error()}
			w.WriteHeader(http.StatusNotFound)
			if err = json.NewEncoder(w).Encode(b); err != nil {
				logger.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
			}
			return
		}
		logger.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusNoContent)
	logger.Printf("User %v updated successful\n", ID)
}

func (u *user) delete(w http.ResponseWriter, r *http.Request) {
	logger := log.New(os.Stderr, fmt.Sprintf("%s - %s :: ", r.Method, r.URL.Path), log.Ldate|log.Ltime|log.Lshortfile)

	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	ID := chi.URLParam(r, "id")
	if err := u.m.Delete(ID); err != nil {
		if ce, ok := err.(*manager.ErrUserNotFound); ok {
			b := errUserNotFound{ID: ce.ID, Message: ce.Error()}
			w.WriteHeader(http.StatusNotFound)
			if err = json.NewEncoder(w).Encode(b); err != nil {
				logger.Println(err)
				w.WriteHeader(http.StatusInternalServerError)
			}
			return
		}
		logger.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusNoContent)
	logger.Printf("User %v updated successful\n", ID)
}

func UserRegister(m manager.User) func(r chi.Router) {
	u := &user{m: m}
	return func(r chi.Router) {
		r.Post("/", u.create)
		r.Get("/", u.list)
		r.Get("/{id}", u.get)
		r.Put("/{id}", u.update)
		r.Delete("/{id}", u.delete)
	}
}
