package resource

import (
	"app/api/manager"
	"fmt"
	"net/http"

	"github.com/go-chi/chi"
)

type auth struct {
	m manager.Auth
}

func (a *auth) generateToken(w http.ResponseWriter, r *http.Request) {
	token, err := a.m.GenerateToken()
	if err != nil {
		panic(err)
	}
	fmt.Println(token)
}

func (a *auth) validToken(w http.ResponseWriter, r *http.Request) {
	ok, err := a.m.ValidToken(r.Header.Get("Authorization"))
	if err != nil {
		panic(err)
	}
	fmt.Println(ok)
}

func AuthRegister(m manager.Auth) func(r chi.Router) {
	a := &auth{m: m}
	return func(r chi.Router) {
		r.Post("/", a.generateToken)
		r.Get("/{token}", a.validToken)
	}
}
