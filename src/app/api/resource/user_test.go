package resource

import (
	"app/api/manager"
	tm "app/api/manager/testing"
	"bytes"
	"context"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi"
)

func TestUserCreateOK(t *testing.T) {
	r := &user{
		m: &tm.UserManager{
			MockCreate: func(_ manager.UserPayload) error {
				return nil
			},
		},
	}

	const body = `{
  "last_name": "Coimbra",
  "first_name": "Arthur",
  "address": "Av. Borges de Medeiros, 997 - Gávea, Rio de Janeiro - RJ, 22430-041",
  "email": "zico@flamengo.com.br",
  "telephone": "21590196"
}`
	w := httptest.NewRecorder()
	rq := httptest.NewRequest(http.MethodPost, "/users", bytes.NewBufferString(body))

	r.create(w, rq)

	if w.Code != http.StatusCreated {
		t.Errorf("unexpected status code: %v", w.Code)
		return
	}
}

func TestUserCreateWithBadRequestBody(t *testing.T) {
	r := &user{m: nil}

	const body = `{
  "address": "Av. Borges de Medeiros, 997 - Gávea, Rio de Janeiro - RJ, 22430-041",
  "email": "zico@flamengo.com.br",
  "telephone": "21590196"
}`
	w := httptest.NewRecorder()
	rq := httptest.NewRequest(http.MethodPost, "/users", bytes.NewBufferString(body))

	r.create(w, rq)

	if w.Code != http.StatusUnprocessableEntity {
		t.Errorf("unexpected status code: %v", w.Code)
		return
	}
}

func TestUserCreateWithEOFFromRequestBody(t *testing.T) {
	r := &user{m: nil}

	const body = ``
	w := httptest.NewRecorder()
	rq := httptest.NewRequest(http.MethodPost, "/users", bytes.NewBufferString(body))

	r.create(w, rq)

	if w.Code != http.StatusInternalServerError {
		t.Errorf("unexpected status code: %v", w.Code)
		return
	}
}

func TestUserCreateFailure(t *testing.T) {
	r := &user{
		m: &tm.UserManager{
			MockCreate: func(_ manager.UserPayload) error {
				return errors.New("test error")
			},
		},
	}

	const body = `{
  "last_name": "Coimbra",
  "first_name": "Arthur",
  "address": "Av. Borges de Medeiros, 997 - Gávea, Rio de Janeiro - RJ, 22430-041",
  "email": "zico@flamengo.com.br",
  "telephone": "21590196"
}`
	w := httptest.NewRecorder()
	rq := httptest.NewRequest(http.MethodPost, "/users", bytes.NewBufferString(body))

	r.create(w, rq)

	if w.Code != http.StatusInternalServerError {
		t.Errorf("unexpected status code: %v", w.Code)
		return
	}
}

func TestUserListOK(t *testing.T) {
	r := &user{
		m: &tm.UserManager{
			MockList: func(_ manager.UserListOptions) ([]manager.UserInfo, error) {
				return nil, nil
			},
		},
	}

	w := httptest.NewRecorder()
	rq := httptest.NewRequest(http.MethodGet, "/users", nil)

	r.list(w, rq)

	if w.Code != http.StatusOK {
		t.Errorf("unexpected status code: %v", w.Code)
		return
	}
}

func TestUserListFailure(t *testing.T) {
	r := &user{
		m: &tm.UserManager{
			MockList: func(_ manager.UserListOptions) ([]manager.UserInfo, error) {
				return nil, errors.New("test error")
			},
		},
	}

	w := httptest.NewRecorder()
	rq := httptest.NewRequest(http.MethodGet, "/users", nil)

	r.list(w, rq)

	if w.Code != http.StatusInternalServerError {
		t.Errorf("unexpected status code: %v", w.Code)
		return
	}
}

func TestUserGetOK(t *testing.T) {
	r := &user{
		m: &tm.UserManager{
			MockGet: func(_ string) (manager.UserInfo, error) {
				return manager.UserInfo{}, nil
			},
		},
	}

	w := httptest.NewRecorder()
	rq := httptest.NewRequest(http.MethodGet, "/users/test", nil)

	rctx := chi.NewRouteContext()

	rq = rq.WithContext(context.WithValue(rq.Context(), chi.RouteCtxKey, rctx))

	r.get(w, rq)

	if w.Code != http.StatusOK {
		t.Errorf("unexpected status code: %v", w.Code)
		return
	}
}

func TestUserGetNotFound(t *testing.T) {
	r := &user{
		m: &tm.UserManager{
			MockGet: func(_ string) (manager.UserInfo, error) {
				return manager.UserInfo{}, &manager.ErrUserNotFound{}
			},
		},
	}

	w := httptest.NewRecorder()
	rq := httptest.NewRequest(http.MethodGet, "/usrs/test", nil)

	rctx := chi.NewRouteContext()

	rq = rq.WithContext(context.WithValue(rq.Context(), chi.RouteCtxKey, rctx))

	r.get(w, rq)

	if w.Code != http.StatusNotFound {
		t.Errorf("unexpected status code: %v", w.Code)
		return
	}
}

func TestUserGetFailure(t *testing.T) {
	r := &user{
		m: &tm.UserManager{
			MockGet: func(_ string) (manager.UserInfo, error) {
				return manager.UserInfo{}, errors.New("test error")
			},
		},
	}

	w := httptest.NewRecorder()
	rq := httptest.NewRequest(http.MethodGet, "/users/test", nil)

	rctx := chi.NewRouteContext()

	rq = rq.WithContext(context.WithValue(rq.Context(), chi.RouteCtxKey, rctx))

	r.get(w, rq)

	if w.Code != http.StatusInternalServerError {
		t.Errorf("unexpected status code: %v", w.Code)
		return
	}
}

func TestUserUpdateOK(t *testing.T) {
	r := &user{
		m: &tm.UserManager{
			MockUpdate: func(_ string, _ manager.UserPayload) error {
				return nil
			},
		},
	}

	const body = `{
  "last_name": "Coimbra",
  "first_name": "Arthur",
  "address": "Av. Borges de Medeiros, 997 - Gávea, Rio de Janeiro - RJ, 22430-041",
  "email": "zico@flamengo.com.br",
  "telephone": "21590196"
}`

	w := httptest.NewRecorder()
	rq := httptest.NewRequest(http.MethodPut, "/users/test", bytes.NewBufferString(body))

	rctx := chi.NewRouteContext()

	rq = rq.WithContext(context.WithValue(rq.Context(), chi.RouteCtxKey, rctx))

	r.update(w, rq)

	if w.Code != http.StatusNoContent {
		t.Errorf("unexpected status code: %v", w.Code)
		return
	}
}

func TestUserUpdateNotFound(t *testing.T) {
	r := &user{
		m: &tm.UserManager{
			MockUpdate: func(_ string, _ manager.UserPayload) error {
				return &manager.ErrUserNotFound{}
			},
		},
	}

	const body = `{
  "last_name": "Coimbra",
  "first_name": "Arthur",
  "address": "Av. Borges de Medeiros, 997 - Gávea, Rio de Janeiro - RJ, 22430-041",
  "email": "zico@flamengo.com.br",
  "telephone": "21590196"
}`

	w := httptest.NewRecorder()
	rq := httptest.NewRequest(http.MethodPut, "/users/test", bytes.NewBufferString(body))

	rctx := chi.NewRouteContext()

	rq = rq.WithContext(context.WithValue(rq.Context(), chi.RouteCtxKey, rctx))

	r.update(w, rq)

	if w.Code != http.StatusNotFound {
		t.Errorf("unexpected status code: %v", w.Code)
		return
	}
}

func TestUserUpdateWithBadRequestBody(t *testing.T) {
	r := &user{
		m: &tm.UserManager{
			MockUpdate: func(_ string, _ manager.UserPayload) error {
				return &manager.ErrUserNotFound{}
			},
		},
	}

	const body = `{
  "last_name": "Coimbra",
  "first_name": "Arthur",
  "address": "Av. Borges de Medeiros, 997 - Gávea, Rio de Janeiro - RJ, 22430-041",
  "telephone": "21590196"
}`

	w := httptest.NewRecorder()
	rq := httptest.NewRequest(http.MethodPut, "/users/test", bytes.NewBufferString(body))

	rctx := chi.NewRouteContext()

	rq = rq.WithContext(context.WithValue(rq.Context(), chi.RouteCtxKey, rctx))

	r.update(w, rq)

	if w.Code != http.StatusUnprocessableEntity {
		t.Errorf("unexpected status code: %v", w.Code)
		return
	}
}

func TestUserUpdateFailure(t *testing.T) {
	r := &user{
		m: &tm.UserManager{
			MockUpdate: func(_ string, _ manager.UserPayload) error {
				return errors.New("test error")
			},
		},
	}

	const body = `{
  "last_name": "Coimbra",
  "first_name": "Arthur",
  "address": "Av. Borges de Medeiros, 997 - Gávea, Rio de Janeiro - RJ, 22430-041",
  "email": "zico@flamengo.com.br",
  "telephone": "21590196"
}`

	w := httptest.NewRecorder()
	rq := httptest.NewRequest(http.MethodPut, "/users/test", bytes.NewBufferString(body))

	rctx := chi.NewRouteContext()

	rq = rq.WithContext(context.WithValue(rq.Context(), chi.RouteCtxKey, rctx))

	r.update(w, rq)

	if w.Code != http.StatusInternalServerError {
		t.Errorf("unexpected status code: %v", w.Code)
		return
	}
}

func TestUserDeleteOK(t *testing.T) {
	r := &user{
		m: &tm.UserManager{
			MockDelete: func(_ string) error {
				return nil
			},
		},
	}

	w := httptest.NewRecorder()
	rq := httptest.NewRequest(http.MethodDelete, "/users/test", nil)

	rctx := chi.NewRouteContext()

	rq = rq.WithContext(context.WithValue(rq.Context(), chi.RouteCtxKey, rctx))

	r.delete(w, rq)

	if w.Code != http.StatusNoContent {
		t.Errorf("unexpected status code: %v", w.Code)
		return
	}
}

func TestUserDeleteNotFound(t *testing.T) {
	r := &user{
		m: &tm.UserManager{
			MockDelete: func(_ string) error {
				return &manager.ErrUserNotFound{}
			},
		},
	}

	w := httptest.NewRecorder()
	rq := httptest.NewRequest(http.MethodDelete, "/users/test", nil)

	rctx := chi.NewRouteContext()

	rq = rq.WithContext(context.WithValue(rq.Context(), chi.RouteCtxKey, rctx))

	r.delete(w, rq)

	if w.Code != http.StatusNotFound {
		t.Errorf("unexpected status code: %v", w.Code)
		return
	}
}

func TestUserDeleteFailure(t *testing.T) {
	r := &user{
		m: &tm.UserManager{
			MockDelete: func(_ string) error {
				return errors.New("test error")
			},
		},
	}

	w := httptest.NewRecorder()
	rq := httptest.NewRequest(http.MethodDelete, "/users/test", nil)

	rctx := chi.NewRouteContext()

	rq = rq.WithContext(context.WithValue(rq.Context(), chi.RouteCtxKey, rctx))

	r.delete(w, rq)

	if w.Code != http.StatusInternalServerError {
		t.Errorf("unexpected status code: %v", w.Code)
		return
	}
}
