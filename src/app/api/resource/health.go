package resource

import (
	"app/api/manager"
	"log"
	"net/http"

	"github.com/go-chi/chi"
)

type health struct {
	m manager.Health
}

func (h *health) check(w http.ResponseWriter, r *http.Request) {
	if err := h.m.Check(); err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusServiceUnavailable)
		return
	}
}

func HealthRegister(m manager.Health) func(r chi.Router) {
	h := &health{m: m}
	return func(r chi.Router) {
		r.Get("/", h.check)
	}
}
