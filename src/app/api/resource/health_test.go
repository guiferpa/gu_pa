package resource

import (
	tm "app/api/manager/testing"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHealthCheckOK(t *testing.T) {
	r := &health{
		m: &tm.HealthManager{
			MockCheck: func() error {
				return nil
			},
		},
	}

	w := httptest.NewRecorder()
	rq := httptest.NewRequest(http.MethodGet, "/health", nil)

	r.check(w, rq)

	if w.Code != http.StatusOK {
		t.Errorf("unexpected status code: %v", w.Code)
		return
	}
}

func TestHealthCheckFailure(t *testing.T) {
	r := &health{
		m: &tm.HealthManager{
			MockCheck: func() error {
				return errors.New("test error")
			},
		},
	}

	w := httptest.NewRecorder()
	rq := httptest.NewRequest(http.MethodGet, "/health", nil)

	r.check(w, rq)

	if w.Code != http.StatusServiceUnavailable {
		t.Errorf("unexpected status code: %v", w.Code)
		return
	}
}
