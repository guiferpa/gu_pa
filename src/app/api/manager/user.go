package manager

import (
	"app/api/service/db"
	"fmt"
)

type UserListOptions struct {
	Offset int
	Limit  int
}

type User interface {
	Create(user UserPayload) error
	List(opts UserListOptions) ([]UserInfo, error)
	Get(ID string) (UserInfo, error)
	Update(ID string, user UserPayload) error
	Delete(ID string) error
}

type UserManager struct {
	db db.Database
}

type UserPayload struct {
	LastName  string
	FirstName string
	Address   string
	Email     string
	Telephone string
}

type UserInfo struct {
	ID        string
	LastName  string
	FirstName string
	Address   string
	Email     string
	Telephone string
}

func getUserPayload(u UserPayload) db.UserPayload {
	return db.UserPayload{
		LastName:  u.LastName,
		FirstName: u.FirstName,
		Address:   u.Address,
		Email:     u.Email,
		Telephone: u.Telephone,
	}
}

func getUserInfo(u db.UserInfo) UserInfo {
	return UserInfo{
		ID:        u.ID,
		LastName:  u.LastName,
		FirstName: u.FirstName,
		Address:   u.Address,
		Email:     u.Email,
		Telephone: u.Telephone,
	}
}

func (um *UserManager) Create(user UserPayload) error {
	if err := um.db.CreateUser(getUserPayload(user)); err != nil {
		return err
	}
	return nil
}

func (um *UserManager) List(opts UserListOptions) ([]UserInfo, error) {
	offset := opts.Offset
	if offset < 0 {
		offset = 0
	}

	limit := opts.Limit
	if limit < 1 || limit > 100 {
		limit = 100
	}

	usersinfo := make([]UserInfo, 0)
	users, err := um.db.ReadUserList(db.ReadUserListOptions{Offset: offset, Limit: limit})
	if err != nil {
		return usersinfo, err
	}
	for _, user := range users {
		usersinfo = append(usersinfo, getUserInfo(user))
	}

	return usersinfo, nil
}

type ErrUserNotFound struct {
	ID string
}

func (e *ErrUserNotFound) Error() string {
	return fmt.Sprintf("user with id %s not found", e.ID)
}

func (um *UserManager) Get(ID string) (UserInfo, error) {
	userinfo, err := um.db.ReadUser(ID)
	if err != nil {
		if err == db.ErrRowNotFound {
			return UserInfo{}, &ErrUserNotFound{ID: ID}
		}
		return UserInfo{}, err
	}
	return getUserInfo(userinfo), nil
}

func (um *UserManager) Update(ID string, user UserPayload) error {
	affecteds, err := um.db.UpdateUser(ID, getUserPayload(user))
	if err != nil {
		return err
	}
	if affecteds < 1 {
		return &ErrUserNotFound{ID: ID}
	}
	return nil
}

func (um *UserManager) Delete(ID string) error {
	affecteds, err := um.db.DeleteUser(ID)
	if err != nil {
		return err
	}
	if affecteds < 1 {
		return &ErrUserNotFound{ID: ID}
	}
	return nil
}

func NewUserManager(dbase db.Database) *UserManager {
	return &UserManager{db: dbase}
}
