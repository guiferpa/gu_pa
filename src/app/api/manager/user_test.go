package manager

import (
	"app/api/service/db"
	tdb "app/api/service/db/testing"
	"errors"
	"fmt"
	"testing"
)

func TestCreateUserOK(t *testing.T) {
	m := &UserManager{
		db: &tdb.DB{
			MockCreateUser: func(_ db.UserPayload) error {
				return nil
			},
		},
	}
	if err := m.Create(UserPayload{}); err != nil {
		t.Error(err)
		return
	}
}

func TestCreateUserFailure(t *testing.T) {
	m := &UserManager{
		db: &tdb.DB{
			MockCreateUser: func(_ db.UserPayload) error {
				return errors.New("test error")
			},
		},
	}
	if err := m.Create(UserPayload{}); err == nil {
		t.Error("expected error")
		return
	}
}

func TestReadUserListOK(t *testing.T) {
	m := &UserManager{
		db: &tdb.DB{
			MockReadUserList: func(_ db.ReadUserListOptions) ([]db.UserInfo, error) {
				rows := []db.UserInfo{
					{ID: "1", LastName: "A", FirstName: "B", Address: "C", Email: "D", Telephone: "E"},
					{ID: "2", LastName: "F", FirstName: "G", Address: "H", Email: "I", Telephone: "J"},
				}
				return rows, nil
			},
		},
	}
	userList, err := m.List(UserListOptions{})
	if err != nil {
		t.Error(err)
		return
	}
	if len(userList) != 2 {
		t.Error("unexpected result length")
		return
	}
}

func TestReadUserListOKWithEmptyValue(t *testing.T) {
	m := &UserManager{
		db: &tdb.DB{
			MockReadUserList: func(_ db.ReadUserListOptions) ([]db.UserInfo, error) {
				return nil, nil
			},
		},
	}
	if _, err := m.List(UserListOptions{}); err != nil {
		t.Error(err)
		return
	}
}

func TestReadUserListOKWithoutLimit(t *testing.T) {
	m := &UserManager{
		db: &tdb.DB{
			MockReadUserList: func(opts db.ReadUserListOptions) ([]db.UserInfo, error) {
				if opts.Limit != 100 {
					return nil, errors.New("unexpected limit")
				}
				return nil, nil
			},
		},
	}
	if _, err := m.List(UserListOptions{}); err != nil {
		t.Error(err)
		return
	}
}

func TestReadUserListOKOffsetOutOfBound(t *testing.T) {
	m := &UserManager{
		db: &tdb.DB{
			MockReadUserList: func(opts db.ReadUserListOptions) ([]db.UserInfo, error) {
				if opts.Offset < 0 {
					return nil, fmt.Errorf("offset out of bound: %v", opts.Offset)
				}
				return nil, nil
			},
		},
	}
	offsets := []int{100000, 1000, 0, -1}
	for _, offset := range offsets {
		if _, err := m.List(UserListOptions{Offset: offset}); err != nil {
			t.Error(err)
			return
		}
	}
}

func TestReadUserListOKLimitOutOfBound(t *testing.T) {
	m := &UserManager{
		db: &tdb.DB{
			MockReadUserList: func(opts db.ReadUserListOptions) ([]db.UserInfo, error) {
				if opts.Limit > 100 || opts.Limit < 1 {
					return nil, fmt.Errorf("limit out of bound: %v", opts.Limit)
				}
				return nil, nil
			},
		},
	}
	limits := []int{100000, 1000, 0, -1}
	for _, limit := range limits {
		if _, err := m.List(UserListOptions{Limit: limit}); err != nil {
			t.Error(err)
			return
		}
	}
}

func TestReadUserListFailure(t *testing.T) {
	m := &UserManager{
		db: &tdb.DB{
			MockReadUserList: func(_ db.ReadUserListOptions) ([]db.UserInfo, error) {
				return nil, errors.New("test error")
			},
		},
	}
	if _, err := m.List(UserListOptions{}); err == nil {
		t.Error("expected error")
		return
	}
}

func TestReadUserOK(t *testing.T) {
	m := &UserManager{
		db: &tdb.DB{
			MockReadUser: func(_ string) (db.UserInfo, error) {
				return db.UserInfo{}, nil
			},
		},
	}
	if _, err := m.Get(""); err != nil {
		t.Error(err)
		return
	}
}

func TestReadUserNotFound(t *testing.T) {
	m := &UserManager{
		db: &tdb.DB{
			MockReadUser: func(_ string) (db.UserInfo, error) {
				return db.UserInfo{}, db.ErrRowNotFound
			},
		},
	}

	id := "A"
	_, err := m.Get(id)
	if err == nil {
		t.Error("expected error")
		return
	}

	ce, ok := err.(*ErrUserNotFound)
	if !ok {
		t.Error("expected the not found error")
		return
	}
	if ce.ID != id {
		t.Error("unexpected id in report of error")
		return
	}
}

func TestReadUserFailure(t *testing.T) {
	m := &UserManager{
		db: &tdb.DB{
			MockReadUser: func(_ string) (db.UserInfo, error) {
				return db.UserInfo{}, errors.New("test erro")
			},
		},
	}
	if _, err := m.Get(""); err == nil {
		t.Error("expected error")
		return
	}
}

func TestUpdateUserOK(t *testing.T) {
	m := &UserManager{
		db: &tdb.DB{
			MockUpdateUser: func(_ string, _ db.UserPayload) (int, error) {
				return 1, nil
			},
		},
	}
	if err := m.Update("", UserPayload{}); err != nil {
		t.Error(err)
		return
	}
}

func TestUpdateUserNotFound(t *testing.T) {
	m := &UserManager{
		db: &tdb.DB{
			MockUpdateUser: func(_ string, _ db.UserPayload) (int, error) {
				return 0, nil
			},
		},
	}

	id := "A"
	err := m.Update(id, UserPayload{})
	if err == nil {
		t.Error("expected error")
		return
	}

	ce, ok := err.(*ErrUserNotFound)
	if !ok {
		t.Error("expected the not found error")
		return
	}
	if ce.ID != id {
		t.Error("unexpected id in report of error")
		return
	}
}

func TestUpdateUserFailure(t *testing.T) {
	m := &UserManager{
		db: &tdb.DB{
			MockUpdateUser: func(_ string, _ db.UserPayload) (int, error) {
				return 0, errors.New("test error")
			},
		},
	}
	if err := m.Update("", UserPayload{}); err == nil {
		t.Error("expected error")
		return
	}
}

func TestDeleteUserOK(t *testing.T) {
	m := &UserManager{
		db: &tdb.DB{
			MockDeleteUser: func(_ string) (int, error) {
				return 1, nil
			},
		},
	}
	if err := m.Delete(""); err != nil {
		t.Error(err)
		return
	}
}

func TestDeleteUserNotFound(t *testing.T) {
	m := &UserManager{
		db: &tdb.DB{
			MockDeleteUser: func(_ string) (int, error) {
				return 0, nil
			},
		},
	}

	id := "A"
	err := m.Delete(id)
	if err == nil {
		t.Error("expected error")
		return
	}

	ce, ok := err.(*ErrUserNotFound)
	if !ok {
		t.Error("expected the not found error")
		return
	}
	if ce.ID != id {
		t.Error("unexpected id in report of error")
		return
	}
}

func TestDeleteUserFailure(t *testing.T) {
	m := &UserManager{
		db: &tdb.DB{
			MockDeleteUser: func(_ string) (int, error) {
				return 0, errors.New("test error")
			},
		},
	}
	if err := m.Delete(""); err == nil {
		t.Error("expected error")
		return
	}
}
