package manager

import "app/api/service/db"

type Health interface {
	Check() error
}

type HealthManager struct {
	db db.Database
}

func (hm *HealthManager) Check() error {
	return hm.db.Ping()
}

func NewHealthManager(dbase db.Database) *HealthManager {
	return &HealthManager{db: dbase}
}
