package testing

type HealthManager struct {
	MockCheck func() error
}

func (hm *HealthManager) Check() error {
	if hm.MockCheck != nil {
		return hm.MockCheck()
	}
	return nil
}
