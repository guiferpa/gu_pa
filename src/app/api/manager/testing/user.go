package testing

import "app/api/manager"

type UserManager struct {
	MockCreate func(user manager.UserPayload) error
	MockList   func(opts manager.UserListOptions) ([]manager.UserInfo, error)
	MockGet    func(ID string) (manager.UserInfo, error)
	MockUpdate func(ID string, user manager.UserPayload) error
	MockDelete func(ID string) error
}

func (um *UserManager) Create(user manager.UserPayload) error {
	if um.MockCreate != nil {
		return um.MockCreate(user)
	}
	return nil
}

func (um *UserManager) List(opts manager.UserListOptions) ([]manager.UserInfo, error) {
	if um.MockList != nil {
		return um.MockList(opts)
	}
	return nil, nil
}

func (um *UserManager) Get(ID string) (manager.UserInfo, error) {
	if um.MockGet != nil {
		return um.MockGet(ID)
	}
	return manager.UserInfo{}, nil
}

func (um *UserManager) Update(ID string, user manager.UserPayload) error {
	if um.MockUpdate != nil {
		return um.MockUpdate(ID, user)
	}
	return nil
}

func (um *UserManager) Delete(ID string) error {
	if um.MockDelete != nil {
		return um.MockDelete(ID)
	}
	return nil
}
