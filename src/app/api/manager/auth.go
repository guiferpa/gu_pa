package manager

import (
	"fmt"

	jwt "github.com/dgrijalva/jwt-go"
)

type Auth interface {
	GenerateToken() (string, error)
	ValidToken(token string) (bool, error)
}

type AuthManager struct {
	secret string
}

type TokenPayload struct {
	LastName  string
	FirstName string
	ExpiresAt int64
}

type tokenClaim struct {
	FullName string `json:"full_name"`
	jwt.StandardClaims
}

func (am *AuthManager) GenerateToken(token TokenPayload) (string, error) {
	claims := tokenClaim{
		fmt.Sprintf("%v %v", token.FirstName, token.LastName),
		jwt.StandardClaims{
			ExpiresAt: token.ExpiresAt,
		},
	}
	t := jwt.NewWithClaims(jwt.SigningMethodHS512, claims)
	return t.SignedString([]byte(am.secret))
}

func (am *AuthManager) ValidToken(tokenString string) (bool, error) {
	token, err := jwt.ParseWithClaims(tokenString, &tokenClaim{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(am.secret), nil
	})
	return token.Valid, err
}

func NewAuthManager(secret string) *AuthManager {
	return &AuthManager{secret: secret}
}
