package manager

import (
	dbt "app/api/service/db/testing"
	"errors"
	"testing"
)

func TestHealthOK(t *testing.T) {
	m := &HealthManager{
		db: &dbt.DB{
			MockPing: func() error {
				return nil
			},
		},
	}
	if err := m.Check(); err != nil {
		t.Error(err)
		return
	}
}

func TestHealthFailure(t *testing.T) {
	m := &HealthManager{
		db: &dbt.DB{
			MockPing: func() error {
				return errors.New("test error")
			},
		},
	}
	if err := m.Check(); err == nil {
		t.Error("expected error")
		return
	}
}
