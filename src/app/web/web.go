package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"

	"app/web/controller"
	"app/web/controller/user"

	"github.com/go-chi/chi"
)

func main() {
	var port, tmpls string
	flag.StringVar(&port, "port", "8080", "set a custom port")
	flag.StringVar(&tmpls, "template", "templates", "set a custom templates directory")

	flag.Parse()

	r := chi.NewRouter()

	r.Get("/", controller.Home(tmpls))
	r.Get("/sign-up", controller.SignUp(tmpls))
	r.Get("/log-in", controller.LogIn(tmpls))
	r.Post("/forget-password", controller.ForgetPassword(tmpls))
	r.Get("/reset-password/{token}", controller.ResetPassword(tmpls))

	r.Route("/user", func(r chi.Router) {
		r.Get("/{id}", user.Profile(tmpls))
	})

	r.NotFound(controller.NotFound(tmpls))
	r.MethodNotAllowed(controller.NotFound(tmpls))

	log.Printf("web app running on :%s", port)

	log.Fatalln(http.ListenAndServe(fmt.Sprintf(":%s", port), r))
}
