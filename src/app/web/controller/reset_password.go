package controller

import (
	"net/http"
	"path/filepath"
	"text/template"

	"github.com/go-chi/chi"
)

type resetPasswordData struct {
	Token string
}

func ResetPassword(tmpls string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		t, err := template.ParseFiles(filepath.Join(tmpls, "reset-password.html"))
		if err != nil {
			panic(err)
		}

		data := resetPasswordData{
			Token: chi.URLParam(r, "token"),
		}
		if err = t.Execute(w, data); err != nil {
			panic(err)
		}
	}
}
