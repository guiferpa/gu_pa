package user

import (
	"net/http"
	"path/filepath"
	"text/template"

	"github.com/go-chi/chi"
)

type profileData struct {
	ID string
}

func Profile(tmpls string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		t, err := template.ParseFiles(filepath.Join(tmpls, "user", "profile.html"))
		if err != nil {
			panic(err)
		}

		data := profileData{
			ID: chi.URLParam(r, "id"),
		}
		if err = t.Execute(w, data); err != nil {
			panic(err)
		}
	}
}
