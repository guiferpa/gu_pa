package controller

import (
	"net/http"
	"path/filepath"
	"text/template"
)

func SignUp(tmpls string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		t, err := template.ParseFiles(filepath.Join(tmpls, "sign-up.html"))
		if err != nil {
			panic(err)
		}

		if err = t.Execute(w, nil); err != nil {
			panic(err)
		}
	}
}
