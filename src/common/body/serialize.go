package body

import (
	"fmt"
	"reflect"
	"strings"
)

type ErrSerialize struct {
	msg string
}

func (err *ErrSerialize) Error() string {
	return err.msg
}

var (
	errUnsupportedStruct = &ErrSerialize{"unsupported struct"}
)

type (
	Tag struct {
		Key   string
		Value string
	}

	Field struct {
		Name  string
		Value string
		Tags  []Tag
	}
)

func Serialize(b interface{}) ([]Field, error) {
	v := reflect.ValueOf(b)
	if v.Kind().String() != "struct" {
		return nil, errUnsupportedStruct
	}
	t := reflect.TypeOf(b)
	fs := make([]Field, 0)
	for i := 0; i < t.NumField(); i++ {
		f := t.Field(i)
		rt := f.Tag.Get("validate")
		if rt == "" {
			continue
		}
		tags := make([]Tag, 0)
		ts := strings.Fields(rt)
		for i := 0; i < len(ts); i++ {
			t := strings.Split(ts[i], "=")
			if len(t) != 2 {
				return nil, &ErrSerialize{fmt.Sprintf("invalid tag for rule: %v", rt)}
			}
			tags = append(tags, Tag{t[0], t[1]})
		}
		fs = append(fs, Field{strings.ToLower(f.Name), fmt.Sprintf("%v", v.FieldByName(f.Name)), tags})
	}
	return fs, nil
}
