package body

import (
	"testing"
)

func TestSerializeBodyStruct(t *testing.T) {
	cases := []struct {
		param interface{}
		ok    bool
	}{
		{map[string]string{"test-key": "test-value"}, false},
		{struct{ Value string }{"test-value"}, true},
		{10, false},
		{struct{}{}, true},
		{"", false},
		{struct {
			Value string `validate:"required"`
		}{"test-value"}, false},
		{struct {
			Value string `validate:"required=true"`
		}{"test-value"}, true},
		{struct {
			Key   string `validate:"testing"`
			Value string `validate:"required=true"`
		}{"test-key", "test-value"}, false},
		{struct {
			Key   string `validate:"testing=test"`
			Value string `validate:"required=true"`
		}{"test-key", "test-value"}, true},
	}

	for _, test := range cases {
		_, err := Serialize(test.param)
		if _, ok := err.(*ErrSerialize); ok == test.ok {
			t.Error(err)
		}
	}
}
