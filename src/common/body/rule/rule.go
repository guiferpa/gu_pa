package rule

type Rule interface {
	Name() string
	Validate(name, value, parameter string) (bool, error)
}

var (
	MinBound = &minBound{}
	MaxBound = &maxBound{}
	Required = &required{}
)
