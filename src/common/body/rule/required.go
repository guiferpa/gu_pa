package rule

import (
	"fmt"
	"strconv"
	"strings"
)

type required struct{}

func (r *required) Name() string {
	return "required"
}

type ErrRequired struct {
	Field string
}

func (err *ErrRequired) Error() string {
	return fmt.Sprintf("%v is required", err.Field)
}

func (r *required) Validate(f, v, p string) (bool, error) {
	b, err := strconv.ParseBool(p)
	if err != nil {
		return false, err
	}
	if b && v != "" {
		return true, nil
	}
	return true, &ErrRequired{strings.ToLower(f)}
}
